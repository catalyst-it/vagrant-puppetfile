# vagrant-puppetfile

This is a small Vagrant plugin that automatically installs any Puppet
modules listed in a Puppetfile during provisioning.

## Installation

    $ vagrant plugin install vagrant-puppetfile

## Usage

To activate the plugin, add the following to your `Vagrantfile`:

    Vagrant.configure('2') do |config|
      # ... other config ...
      config.vm.provision 'puppetfile'
      # ...
    end

You can also trigger module installation manually with the
`vagrant puppetfile` command:

    $ vagrant puppetfile --help
    Usage: vagrant puppetfile install
    
    Options:
        -h, --help                       Print this help
        -e, --evaluator TYPE             Specify the Puppetfile evaluator
        -f, --file PUPPETFILE            Specify an alternative Puppetfile

## How it works

The plugin simply calls out to an external tool to fetch and install
dependencies from your Puppetfile while provisioning each machine.

If either one of [`librarian-puppet`][1] or [`r10k`][2] is found on the
host, that will be used. Otherwise, a very basic fallback interpreter
will read your Puppetfile and call `puppet-module(8)` to install the
modules listed therein.

Not all [Puppetfile][3] [directives][4] are supported by this fallback
interpreter. Currently, the following features *will* work:

  - `forge 'http://puppet.forge.url/'`
  - `moduledir 'path'`
  - `mod 'plugin/name'`
  - `mod 'plugin/name', 'version'`

Other directives (such as `metadata`) are unsupported and will signal an
error.

[1]: https://github.com/voxpupuli/librarian-puppet
[2]: https://github.com/puppetlabs/r10k
[3]: https://github.com/voxpupuli/librarian-puppet#the-puppetfile
[4]: https://github.com/puppetlabs/r10k/blob/master/doc/puppetfile.mkd#puppetfile

## Configuration

You can specify which Puppetfile evaluator to use with the `evaluator`
setting:

    config.vm.provision 'puppetfile' do |puppetfile|
      puppetfile.evaluator = '/path/to/librarian-puppet' # or 'r10k' or 'puppet'
    end

## Hacking

Use Bundler to install dependencies:

    $ bundle install

The tests are run with RSpec:

    $ bundle exec rspec

To run vagrant with the plugin automatically loaded, you can use the
`bundle exec` command:

    $ bundle exec vagrant <command>

For example, you can test the `vagrant puppetfile` command directly
by running:

    $ bundle exec vagrant puppetfile install
