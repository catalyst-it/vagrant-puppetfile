#
# Copyright (c) 2016-2017 Catalyst.net Ltd
#
# This file is part of vagrant-puppetfile.
#
# vagrant-puppetfile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# vagrant-puppetfile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-puppetfile. If not, see
# <http://www.gnu.org/licenses/>.
#

require 'logger'
require 'open3'
require 'optparse'
require 'shellwords'

module Vagrant
  module Puppetfile
    DEFAULT_PUPPETFILE_PATH = 'Puppetfile'
    DEFAULT_PUPPETFILE_EVALUATOR = 'autodetect'

    class Provisioner < Vagrant.plugin('2', :provisioner)
      def initialize(machine, config)
        @config = config
        @logger = Vagrant::UI::Prefixed.new(machine.env.ui, machine.name)
      end

      def provision
        Evaluator.create(@config.evaluator, @logger).install(@config.path)
      rescue Exception => e
        @logger.error("Failed to evaluate Puppetfile: #{e.message}")
      end
    end

    class Command < Vagrant.plugin('2', :command)
      def execute
        puppetfile = DEFAULT_PUPPETFILE_PATH
        evaluator  = DEFAULT_PUPPETFILE_EVALUATOR

        options = OptionParser.new do |o|
          o.banner = 'Usage: vagrant puppetfile install'
          o.separator ''
          o.separator 'Options:'
          o.on('-h', '--help', 'Print this help') do
            return @env.ui.info(o.help)
          end
          o.on('-e', '--evaluator TYPE', 'Specify the Puppetfile evaluator') do |type|
            evaluator = type
          end
          o.on('-f', '--file PUPPETFILE', 'Specify an alternative Puppetfile') do |path|
            puppetfile = path
          end
        end

        case parse_options(options)
        when ['install']
          Evaluator.create(evaluator, @env.ui).install(puppetfile)
        else
          @env.ui.warn(options.help)
          exit 1
        end
      end

      def Command.synopsis
        'installs all modules listed in a Puppetfile'
      end
    end

    class Config < Vagrant.plugin('2', :config)
      attr_accessor :path
      attr_accessor :evaluator

      def initialize
        @path = UNSET_VALUE
        @evaluator = UNSET_VALUE
      end

      def validate(*_)
        errors = _detected_errors
        errors << 'path must be a String' unless @path.is_a? String
        errors << 'evaluator must be a String' unless @evaluator.is_a? String
        Hash[Vagrant::Puppetfile::Plugin.name => errors]
      end

      def finalize!
        @path = DEFAULT_PUPPETFILE_PATH if @path == UNSET_VALUE
        @evaluator = DEFAULT_PUPPETFILE_EVALUATOR if @evaluator == UNSET_VALUE
      end
    end

    class Plugin < Vagrant.plugin('2')
      name 'puppetfile'
      config(:puppetfile, :provisioner) { Config }
      command(:puppetfile) { Command }
      provisioner(:puppetfile) { Provisioner }
    end

    module Evaluator
      EVALUATORS = %w(librarian-puppet r10k puppet)

      class InstallationError < Vagrant::Errors::VagrantError
        def error_message
          'Puppetfile module installation failed'
        end
      end

      class ConfigurationError < Vagrant::Errors::VagrantError
        def error_message
          'Invalid vagrant-puppetfile plugin configuration'
        end
      end

      class EvaluatorError < Vagrant::Errors::VagrantError
        def error_message
          'No Puppetfile evaluator was found'
        end
      end

      def Evaluator.create(type, logger)
        case File.basename(type)
        when 'librarian-puppet'
          Librarian.new(type, logger)
        when 'r10k'
          R10k.new(type, logger)
        when 'puppet'
          Puppet.new(type, logger)
        when 'autodetect'
          autodetect(logger) or fail ConfigurationError
        else
          logger.warn("Invalid evaluator: #{type}")
          fail ConfigurationError, type
        end
      end

      def Evaluator.autodetect(logger)
        EVALUATORS.detect do |type|
          begin
            return create(type, logger)
          rescue EvaluatorError => e
            nil
          end
        end
      end

      class Base
        attr_reader :logger
        attr_reader :path

        ENV_CLEAN_KEYS = %w(GEM_HOME GEM_PATH)

        def initialize(path, logger)
          @logger = logger
          @path = path
          # Make sure the evaluator actually exists and can be called.
          unless available?
            logger.warn("Unable to find command: #{path}")
            fail EvaluatorError, path
          end
        end

        def validate(puppetfile)
          # Check for the puppetfile and bomb if it doesn't exist.
          unless File.readable?(puppetfile)
            logger.warn("Unable to read puppetfile: #{puppetfile}")
            fail ConfigurationError, puppetfile
          end
        end

        def with_clean_env
          env = ENV_CLEAN_KEYS.map { |k| [k, ENV.delete(k)] }
          if not defined? ::Bundler
            yield
          else
            ::Bundler.with_clean_env do
              yield
            end
          end
        ensure
          env.each { |(k, v)| ENV[k] = v }
        end

        def command(*args)
          with_clean_env do
            Open3.capture2e(path, *args.map(&:shellescape))
          end
        end

        def run(*args)
          output, status = command(*args)
          if status.success?
            output.lines.each { |l| logger.detail(l.chomp) }
          else
            output.lines.each { |l| logger.error(l.chomp) }
            fail InstallationError, output
          end
        end
      end

      class R10k < Base
        def available?
          command('version').last.success?
        rescue SystemCallError
          false
        end

        def install(puppetfile)
          validate(puppetfile)
          run('puppetfile', 'install', '--verbose')
        end
      end

      class Librarian < Base
        SUPPORTED_PUPPETFILE_NAMES = %w{Puppetfile Modulefile metadata.json}

        def validate(puppetfile)
          super
          unless SUPPORTED_PUPPETFILE_NAMES.include? File.basename(puppetfile)
            logger.error("The 'librarian-puppet' evaluator cannot install from a file named #{puppetfile.inspect}")
            logger.error("Please use one of these names instead: #{SUPPORTED_PUPPETFILE_NAMES.map(&:inspect).join(', ')}")
            fail ConfigurationError, puppetfile
          end
        end

        def available?
          command('version').last.success?
        rescue SystemCallError
          false
        end

        def install(puppetfile)
          validate(puppetfile)
          dir = File.dirname(puppetfile)
          Dir.chdir(dir) do
            run('install', '--verbose')
          end
        end
      end

      class Puppet < Base
        def initialize(path, logger)
          super
          @moduledir = 'modules'
        end

        def available?
          command('--version').last.success?
        rescue SystemCallError
          false
        end

        def install(puppetfile)
          validate(puppetfile)
          logger.detail("Notice: Evaluating #{puppetfile} ...")
          instance_eval(File.read(puppetfile))
        end

        def unsupported(option, method, *args)
          option = option.inspect
          args = args.map(&:inspect).join(', ')
          logger.error("Unsupported option #{option} in `#{method} #{args}'")
          fail InstallationError, path
        end

        def moduledir(moduledir)
          @moduledir = moduledir
        end

        def forge(forge)
          @forge = forge
        end

        def mod(name, version = :latest, options = {})
          if version.is_a?(Hash)
            options, version = version, :latest
          end

          unless options.is_a?(Hash) and options.empty?
            unsupported(options, 'mod', name, version, options)
          end

          unless version.is_a?(String) or version == :latest
            unsupported(version, 'mod', name, version)
          end

          module_option  = '--target-dir', @moduledir
          forge_option   = '--module_repository', @forge if @forge.is_a? String
          version_option = '--version', version if version.is_a? String

          run('module', 'install', '--verbose', name, *module_option, *forge_option, *version_option)
        end
      end
    end
  end
end
