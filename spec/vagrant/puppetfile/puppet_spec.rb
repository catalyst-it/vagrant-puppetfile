#
# Copyright (c) 2017 Catalyst.net Ltd
#
# This file is part of vagrant-puppetfile.
#
# vagrant-puppetfile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# vagrant-puppetfile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-puppetfile. If not, see
# <http://www.gnu.org/licenses/>.
#

describe Evaluator::Puppet do
  let(:logger) { BlankObject.new }

  context 'assuming puppet is available' do
    before do
      allow(Open3).to receive(:capture2e).with('puppet', any_args).and_return([String.new, SuccessObject.new])
    end

    it 'should create an instance' do
      expect(described_class.new('puppet', logger)).to be_a described_class
    end

    context 'with an instance' do
      let(:instance) { described_class.new('puppet', logger) }

      it 'should be available' do
        expect(instance.available?).to be true
      end

      context 'with a blank Puppetfile' do
        let(:puppetfile) { File.expand_path('Puppetfile.empty', __dir__) }

        it 'should install successfully' do
          expect(instance.install(puppetfile))
        end
      end
    end
  end

  context 'with puppet available', evaluators: ['puppet'] do
    let(:logger) { FakeLogger.new }

    around do |example|
      Dir.chdir(tmpdir = Dir.mktmpdir) do
        example.run
        FileUtils.rm_rf(tmpdir)
      end
    end

    it 'should create an instance' do
      expect(described_class.new('puppet', logger)).to be_a described_class
    end

    context 'with an instance' do
      let(:instance) { described_class.new('puppet', logger) }

      it 'should be available' do
        expect(instance.available?).to be true
      end

      context 'with a simple Puppetfile' do
        let(:puppetfile) { File.expand_path('Puppetfile.typical', __dir__) }

        it 'should install successfully' do
          expect { instance.install(puppetfile) }.not_to raise_exception
          expect(logger.output).to include "Notice: Evaluating #{puppetfile} ..."
          expect(Dir['modules/*']).to contain_exactly 'modules/stdlib'
          expect(Dir['modules/stdlib/*']).to include 'modules/stdlib/metadata.json'
        end
      end
    end
  end
end
